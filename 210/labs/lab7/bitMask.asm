.data
	str:		.asciiz 	"Enter a float value: \n"
	hex:		.asciiz		"In hex: "
	float:		.float		0.0
	binary:		.asciiz		"\nIn binary: "
	one:		.float		1
	newLine:		.asciiz		"\n"
	signP:		.asciiz 	"Sign is +\n"
	signN:		.asciiz		"Sign is -\n"	
	exp:		.ascii		"Exponent: 2^"
	ttt:		.float		8388608.0
	significand:	.asciiz		"Signifcand is: "

.text

	lwc1  $f4, float
	#display message
	li $v0, 4
	la $a0, str
	syscall 
	
	#read user input
	li $v0, 6
	syscall
	
	
	#CONVERT TO HEX
	mfc1 $t0, $f0
	la $a0, hex
	li $v0, 4
	syscall
	
	move $a0, $t0
	li $v0, 34
	syscall
	

	
	#CONVERT TO BINARY
	la $a0, binary
	li $v0, 4
	syscall
	li $v0, 35
	move $a0, $t0
	syscall
	li $v0, 4
	la $a0, newLine
	syscall
	

	
	#DERTEMINE SIGN 
	li $t9, 0x80000000	#mask
	and $t1, $t0, $t9	#compare input to mask
	
	beq $zero, $t1, pos	#if sign is not zero jump to pos
	bne $zero, $t1, nega	#if sign is zero jump to nega
	

	#DETERMINE EXPONENT 
EXPONENT:
	li $t9, 0x7f800000	#mask
	and $t2, $t0, $t9	#compare input to mask
	la $a0, exp	
	li $v0, 4
	syscall
	
	srl $t2, $t2, 23	#shift right
	li $s0, 127
	sub $t3, $t2, $s0	#subtract by 127
	li $v0, 1
	move $a0, $t3
	syscall
	
	li $v0, 4
	la $a0, newLine
	syscall 
	
	#DETERMINE SIGNIFCAND 
	li $t9, 0x3fffff	#mask
	
	and $t4, $t0, $t9	#comapre input to mask
	mtc1 $t4, $f1		#moving the masked to float
	cvt.s.w $f1, $f1	#storing $f1, as a single precision
	l.s $f2, ttt		#loading 2^23
	l.s $f3, one		#loading the one
	div.s $f1, $f1, $f2	#dividing the masked by 2^23
	add.s $f1, $f1, $f3	#adding the one to the masked
	mov.s $f12, $f1		#moving to print
	
	la $a0, significand
	li $v0, 4
	syscall
	
	li $v0, 2
	mov.s $f12, $f1		#moving to print
	syscall
	
	li $v0, 10
	syscall
	
	
pos:
	#IF SIGN IS POSITIVE
	li $v0, 4
	la $a0, signP
	syscall
	
	j EXPONENT
	
nega:
	#IF SIGN IS NEGATIVE
	li $v0, 4
	la $a0, signN
	syscall
	
	j EXPONENT
	
	
	
	
	
	
