#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main(){
	//declare the for loop counter
	//and float variables to hold the answers for
	//each column
    int number, i;
    float oneThird, square, logThree, onePointTwo;

    printf("Enter an number between 1 and 50: ");
    scanf("%d", &number);
    printf("%d", number);

    if((number>50) || (number<0)){
    printf("\n");
    printf("\tx\t 1/x^3\t   sqrt(x)\tlog_3(x)\t 1.2^x\n");
    printf("\t-\t -----\t   -------\t--------\t ------\n");
    }
    if(number > 50) {
        printf("Too high! Try again\n");
        exit(0);
    } if(number < 0){
        printf("Too low! Try again\n");
        exit(1);
    }



    for(i = 1; i <= number; i++){
	//use a for loop to do all the calculations for column all in one line
	//going from 1 to 4
        printf("\t%d\t", i);
        oneThird = 1/(float)(pow(i,3));
		square = (float)sqrt(i);
		logThree = (float)(log(i)/log(3));
		onePointTwo = (float)(pow(1.2,i));
        printf("%.5f\t   %.5f\t %.5f\t%.5f\n", oneThird, square, logThree, onePointTwo);
    }
	//break out of the loop to print the separating line
	//implement the same calculations again with number instead of i
		printf("       ...\t  ...\t     ...      \t   ...     \t  ...\n");
 		oneThird = 1/(float)(pow(number,3));
		square = (float)sqrt(number);
		logThree = (float)(log(number)/log(3));
		onePointTwo = (float)(pow(1.2,number));
		printf("\t%d\t%.5f\t   %.5f\t %.5f\t%.5f\n", number, oneThird, square, logThree, onePointTwo);
    return 0;
}

