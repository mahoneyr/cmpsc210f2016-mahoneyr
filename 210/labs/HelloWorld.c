#include <stdio.h>
//pulls in external libraries to be used
//specifically the Standard Input/Output library

int main() {
    //the main method is to return an integer, in this case 0
   // String myString = "HelloWorld!";
    printf("Hello World!");
    //prints the string in the terminal
        return 0;
        //returns a 0 as required by the method
}
