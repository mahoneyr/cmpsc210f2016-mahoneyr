#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main() {
    int i, max, first, second, third, fourth;

    //use char array to store equations
    char maths[4][30] = {"1/x^3", "sqrt(x)", "log_3(x)", "1.2^x"};
    //use int array to keep track of values chosen
    int chosen[4];

    //prompt first option
     printf("Which column should appear first?\n");
    for(i = 1; i < 5; i++){
      printf("%d.\t", i);
      printf("%s\n", maths[i-1]);
    }
    //store first chosen number
    scanf("%d",&first);
    chosen[0] = first;


    //ask for second input
    //iterate through for loop to determine what options are left
    printf("Which column should appear second?\n");
     for(i = 0; i < 4; i++){
        if(chosen[i] == 1){
        printf("1.\t%s\n2.\t%s\n3.\t%s\n", maths[1],maths[2],maths[3]);
        }
        if(chosen[i] == 2){
        printf("1.\t%s\n2.\t%s\n3.\t%s\n", maths[0],maths[2],maths[3]);
        }
        if(chosen[i] == 3){
        printf("1.\t%s\n2.\t%s\n3.\t%s\n", maths[0],maths[1],maths[3]);
        }
        if(chosen[i] == 4){
        printf("1.\t%s\n2.\t%s\n3.\t%s\n", maths[0],maths[1],maths[2]);
        }
     }
    scanf("%d",&second);

    //adjust for the change in numbering
    if((chosen[0] == 2) && (second == 2)){
        second = second + 1;
    }
     if((chosen[0] == 1) && (second == 3)){
        second = second + 1;
    }
    if((chosen[0] == 3) && (second == 3)){
        second = second + 1;
    }
    if((chosen[0] == 2) && (second == 3)){
        second = second + 1;
    }
     if((chosen[0] == 1) && (second == 2)){
        second = second + 1;
    }
      if((chosen[0] == 1) && (second == 1)){
        second = second + 1;
    }

      //prompt and store 3rd response
    printf("\nWhich column should appear third?\n");
    //////////1 combinations////////////////////////////
    if((chosen[0] == 1) && (chosen[1] == 2)){
        printf("1.\t%s\n2.\t%s\n", maths[2], maths[3]);
          }
    if((chosen[0] == 2) && (chosen[1] == 1)){
        printf("1.\t%s\n2.\t%s\n", maths[2], maths[3]);
          }

    if((chosen[0] == 1) && (chosen[1] == 3)){
        printf("1.\t%s\n2.\t%s\n", maths[1], maths[3]);
          }
     if((chosen[0] == 3) && (chosen[1] == 1)){
        printf("1.\t%s\n2.\t%s\n", maths[1], maths[3]);
          }

       if((chosen[0] == 1) && (chosen[1] == 4)){
        printf("1.\t%s\n2.\t%s\n", maths[1], maths[2]);
          }
     if((chosen[0] == 4) && (chosen[1] == 1)){
        printf("1.\t%s\n2.\t%s\n", maths[1], maths[2]);
          }
     /////////2 combinations/////////////////////////////
      if((chosen[0] == 2) && (chosen[1] == 3)){
        printf("1.\t%s\n2.\t%s\n", maths[0], maths[3]);
          }
      if((chosen[0] == 3) && (chosen[1] == 2)){
        printf("1.\t%s\n2.\t%s\n", maths[0], maths[3]);
          }
         if((chosen[0] == 2) && (chosen[1] == 4)){
        printf("1.\t%s\n2.\t%s\n", maths[0], maths[2]);
          }
          if((chosen[0] == 4) && (chosen[1] == 2)){
        printf("1.\t%s\n2.\t%s\n", maths[0], maths[2]);
          }
        ///////////3 combinations/////////////////////////
         if((chosen[0] == 3) && (chosen[1] == 4)){
        printf("1.\t%s\n2.\t%s\n", maths[0], maths[1]);
          }
         if((chosen[0] == 4) && (chosen[1] == 3)){
        printf("1.\t%s\n2.\t%s\n", maths[0], maths[1]);
          }
         scanf("%d", &third);

        //adjust numbering to be stored in chosen[]
        if((chosen[0] == 1) && (chosen[1] == 2)){
            third = third +2;
        }
         if((chosen[0] == 2) && (chosen[1] == 1)){
            third = third +2;
        }
        if((chosen[0] == 2) && (chosen[1] == 3)){
            third = third +2;
        }
        if((chosen[0] == 1) && (chosen[1] == 3)){
            third = third +1;
        }
        if((chosen[0] == 3) && (chosen[1] == 1)){
            third = third + 1;
        }
        if((chosen[0] == 3) && (chosen[1] == 2)){
            third = third + 1;
        }
        if((chosen[0] == 2) && (chosen[1] == 3)){
            third = third + 1;
        }
        chosen[2] = third;


        //display the last remaining equation based on
        //what is in chosen[]
        if((chosen[0] == 1) && (chosen[1] == 2) && (chosen[2] == 3)){
         printf("\n1.2^x will be the fourth column\n");
         chosen[3] = 4;
        }
        if((chosen[0] == 1) && (chosen[1] == 3) && (chosen[2] == 2)){
           printf("\n1.2^x will be the fourth column\n");
           chosen[3] = 4;
        }
        if((chosen[0] == 2) && (chosen[1] == 1) && (chosen[2] == 3)){
           printf("\n1.2^x will be the fourth column\n");
           chosen[3] = 4;
        }
        if((chosen[0] == 2) && (chosen[1] == 3) && (chosen[2] == 1)){
           printf("\n1.2^x will be the fourth column\n");
           chosen[3] = 4;
        }
        if((chosen[0] == 3) && (chosen[1] == 2) && (chosen[2] == 1)){
           printf("\n1.2^x will be the fourth column\n");
           chosen[3] = 4;
        }
        if((chosen[0] == 3) && (chosen[1] ==1) && (chosen[2] == 2)){
           printf("\n1.2^x will be the fourth column\n");
           chosen[3] = 4;
        }
        /////////////////////////////////////////////////////////

         if((chosen[0] == 1) && (chosen[1] == 2) && (chosen[2] == 4)){
            printf("\nlog_3(x) will be the fourth column\n");
            chosen[3] = 3;
        }
        if((chosen[0] == 1) && (chosen[1] == 4) && (chosen[2] == 2)){
             printf("\nlog_3(x) will be the fourth column\n");
             chosen[3] = 3;
        }
        if((chosen[0] == 2) && (chosen[1] == 1) && (chosen[2] == 4)){
             printf("\nlog_3(x) will be the fourth column\n");
             chosen[3] = 3;
        }
        if((chosen[0] == 2) && (chosen[1] == 4) && (chosen[2] == 1)){
            printf("\nlog_3(x) will be the fourth column\n");
            chosen[3] = 3;
        }
        if((chosen[0] == 4) && (chosen[1] == 2) && (chosen[2] == 1)){
            printf("\nlog_3(x) will be the fourth column\n");
            chosen[3] = 3;
        }
        if((chosen[0] == 4) && (chosen[1] ==1) && (chosen[2] == 2)){
            printf("\nlog_3(x) will be the fourth column\n");
            chosen[3] = 3;
        }
///////////////////////////////////////////////////////////////////

        if((chosen[0] == 1) && (chosen[1] == 3) && (chosen[2] == 4)){
            printf("\nsqrt(x) will be the fourth column\n");
            chosen[3] = 2;
        }
        if((chosen[0] == 1) && (chosen[1] == 4) && (chosen[2] == 3)){
            printf("\nsqrt(x) will be the fourth column\n");
            chosen[3] = 2;
        }
        if((chosen[0] == 3) && (chosen[1] == 1) && (chosen[2] == 4)){
            printf("\nsqrt(x) will be the fourth column\n");
            chosen[3] = 2;
        }
        if((chosen[0] == 3) && (chosen[1] == 4) && (chosen[2] == 1)){
             printf("\nsqrt(x) will be the fourth column\n");
             chosen[3] = 2;
        }
        if((chosen[0] == 4) && (chosen[1] == 3) && (chosen[2] == 1)){
             printf("\nsqrt(x) will be the fourth column\n");
             chosen[3] = 2;
        }
        if((chosen[0] == 4) && (chosen[1] ==1) && (chosen[2] == 3)){
             printf("\nsqrt(x) will be the fourth column\n");
             chosen[3] = 2;
        }
    //////////////////////////////////////////////////////////////////
         if((chosen[0] == 2) && (chosen[1] == 3) && (chosen[2] == 4)){
            printf("\n1/x^3 will be the fourth column\n");
            chosen[3] = 1;
        }
        if((chosen[0] == 2) && (chosen[1] == 4) && (chosen[2] == 3)){
            printf("\n1/x^3 will be the fourth column\n");
            chosen[3] = 1;
        }
        if((chosen[0] == 3) && (chosen[1] == 2) && (chosen[2] == 4)){
            printf("\n1/x^3 will be the fourth column\n");
            chosen[3] = 1;
        }
        if((chosen[0] == 3) && (chosen[1] == 4) && (chosen[2] == 2)){
            printf("\n1/x^3 will be the fourth column\n");
            chosen[3] = 1;
        }
        if((chosen[0] == 4) && (chosen[1] == 3) && (chosen[2] == 2)){
             printf("\n1/x^3 will be the fourth column\n");
             chosen[3] = 1;
        }
        if((chosen[0] == 4) && (chosen[1] ==2) && (chosen[2] == 3)){
            printf("\n1/x^3 will be the fourth column\n");
            chosen[3] = 1;
        }



    //prompt for max int to use
	printf("Enter an integer between 1 and 50:");
	scanf("%d",&max);

    //print out the right equation order
    //based on what in chosen[]
    printf("\t x");

         if(chosen[0] == 1){
            printf("\t 1/x^3");
         } else if(chosen[0] == 2){
             printf("\t sqrt(x)");
         } else if(chosen[0] == 3){
             printf("\t log_3(x)");
         } else if (chosen[0] == 4){
             printf("\t 1.2^x");
         }

         if(chosen[1] == 1){
            printf("\t 1/x^3");
         } else if(chosen[1] == 2){
             printf("\t sqrt(x)");
         } else if(chosen[1] == 3){
             printf("\t log_3(x)");
         } else if (chosen[1] == 4){
             printf("\t 1.2^x");
         }

         if(chosen[2] == 1){
            printf("\t 1/x^3");
         } else if(chosen[2] == 2){
             printf("\t sqrt(x)");
         } else if(chosen[2] == 3){
             printf("\t log_3(x)");
         } else if (chosen[2] == 4){
             printf("\t 1.2^x");
         }

         if(chosen[3] == 1){
            printf("\t 1/x^3");
         } else if(chosen[3] == 2){
             printf("\t sqrt(x)");
         } else if(chosen[3] == 3){
             printf("\t log_3(x)");
         } else if (chosen[3] == 4){
             printf("\t 1.2^x");
         }
         printf("\n");
         printf("\t -\t-------  \t --------  \t--------\t   --------\n");

         //perform the calculations based on what
         //is inside chosen[]
         for (i = 1; i <= max; i++) {
             printf("\t %d", max);

            if(chosen[0] == 1){
            printf("\t%2f", pow(i,-3));
            } else if(chosen[0] == 2){
             printf("\t %1.5f", sqrt(i));
            } else if(chosen[0] == 3){
             printf("\t %1.5f",log(i)/log(3) );
            } else if (chosen[0] == 4){
             printf("\t %10.5f", pow(1.2,i));
            }

            if(chosen[1] == 1){
            printf("\t%2f", pow(i,-3));
            } else if(chosen[1] == 2){
             printf("\t %1.5f", sqrt(i));
            } else if(chosen[1] == 3){
             printf("\t %1.5f",log(i)/log(3) );
            } else if (chosen[1] == 4){
             printf("\t %10.5f", pow(1.2,i));
            }

            if(chosen[2] == 1){
            printf("\t%2f", pow(i,-3));
            } else if(chosen[2] == 2){
             printf("\t %1.5f", sqrt(i));
            } else if(chosen[2] == 3){
             printf("\t %1.5f",log(i)/log(3) );
            } else if (chosen[2] == 4){
             printf("\t %10.5f", pow(1.2,i));
            }

            if(chosen[3] == 1){
            printf("\t%2f", pow(i,-3));
            } else if(chosen[3] == 2){
             printf("\t %1.5f", sqrt(i));
            } else if(chosen[3] == 3){
             printf("\t %1.5f",log(i)/log(3) );
            } else if (chosen[3] == 4){
             printf("\t %10.5f", pow(1.2,i));
            }
            printf("\n");

            //NOTE: I was not able to find a way to keep
            //the alignments from messing up slightly.
            //But i figured since its 11pm on Tuesday it
            //might be more beneficial to start on the
            //MIPS part
	} //for
   	return 0;
} //main

