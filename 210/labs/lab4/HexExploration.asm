.data
	hello:		.asciiz		"Hello world!\n"
	number1:	.word		42
	number2:	.half		21
	number3:	.half		1701
	number4:	.byte		73
	number5:	.half		-1701
	number6:	.byte		127
	number7:	.word		65536
	sum:		.word		0
	diffrence:		.word		0

	
	#######################
	#  ADDRESS   #  DATA  #
	#######################
	# 0x10010010 # 42      	#
	# 0x10010011 # 42      	#
	# 0x10010012 # 42       	#
	# 0x10010013 # 42       	#
	# 0x10010014 # 21       	#
	# 0x10010015 # 21       	#
	# 0x10010016 # 1701   	#
	# 0x10010017 #  1701   	#
	# 0x10010018 # 73       	#
	# 0x10010019 # NA       	#
	# 0x1001001a #  -1701     #
	# 0x1001001b #   -1701   	#
	# 0x1001001c #   127     	#
	# 0x1001001d #   NA    	#
	# 0x1001001e #    NA    	#
	# 0x1001001f #    NA    	#
	# 0x10010020 #   65536   	#
	# 0x10010021 #   65536  	#
	# 0x10010022 #   65536 	#
	# 0x10010023 #   65536  	#
	# 0x10010024 #   NA     	#
	# 0x10010025 #   NA    	#
	# 0x10010026 #   NA     	#
	# 0x10010027 #   NA     	#
	#######################


.text

	la $a0, hello
	li $v0, 4
	syscall
	
	lw $t1, number1		#loads .word 42 into $t1
	lh $t2, number2		#loads .half 21 into $t2
	lh $t3, number3		#loads .half 1701 into $t3
	lb $t4, number4		#loads .byte 73 into $t4
	lh $t5, number5		#loads .half -1701 into $t5
	lb $t6, number6		#loads .byte 127 into $t6
	lw $t7, number7		#loads .word 65536 into $t7
	
	
	add $t8, $t5, $t3	#adds the value in register $t5 and $t3 and stores it in $t8
	sw $t8, sum		#stores the value in the $t8 register into sum
	sub $t9, $t5, $t3	#subtracts the value in $t5 and $t3 and stores it in $t9
	sw $t9, diffrence	#stores the value in the $t9 register into diffrence
	
	
	li $v0, 10
	syscall
