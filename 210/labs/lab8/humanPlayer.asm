.data
	prompt:		.asciiz 	"Welcome to this Guessing Game, human!\nIm thinking of a number between 1 and 100.\nSee if you can guess it!\nYour Guess: "
	TooHigh:		.asciiz		"Your guess was too high!"
	TooLow:		.asciiz		"Your guess was too low!"
	MyNum:		.asciiz		"My number was: "
	newLine:		.asciiz		"\n"
	YourGuess:	.asciiz		"\nYour Guess: "
	Sucess1:		.asciiz		"Thats it!\nOnly took you "
	Sucess2:		.asciiz		" tries! "
	Again:		.asciiz		"\nWould you like to play again? (y/n)"
	answer:		.space 		256
.text
MAIN:

	li $t3, 0	#initalize counter
	
	jal WELCOME
	jal INPUT
	jal DETERMINE
	jal EXIT

WELCOME:
	# prologue
    	subi $sp, $sp, 4
    	sw $ra, 0($sp)
	
    	
    	li $a1, 101	#generatwe the random number
    	li $a0, 0
    	li $v0, 42
    	syscall
    	
    
    	move $t0, $a0		#save random number in $t0
    	
	# procedure body
    	li $v0, 4
    	la $a0, prompt
    	syscall
    	
    	# epilogue
    	lw $ra, 0($sp)
    	addi $sp, $sp, 4
    	
    	
    	jr $ra
    	
 INPUT:
 	# prologue
    	subi $sp, $sp, 4
    	sw $ra, 0($sp)
    	
    	li $v0, 5			# Get input from user
    	syscall
    	move $t1, $v0			#save input in $t1
    	
    	addi $t3, $t3, 1		#inciment counter
    	
    	
    	# epilogue
    	lw $ra, 0($sp)
    	addi $sp, $sp, 4
  
    	jr $ra
    	
DETERMINE:
	
   	blt $t1, $t0, low	#branch if input is too low
   	bgt $t1, $t0, high	#branch if input is too high
   	beq $t1, $t0, equal	#branch if input is too high
 
 
low:	#output that guess was too low
	li $v0, 4
   	la $a0, TooLow
    	syscall
    	
   	j INPUTAGAIN
    	
 high:
 	#output guess was too high
	li $v0, 4
    	la $a0, TooHigh
    	syscall
    	
   	j INPUTAGAIN
   	
equal:
	# prologue
    	subi $sp, $sp, 4
    	sw $ra, 0($sp)
    	
    	#Display sucess message
	li $v0, 4
    	la $a0, Sucess1
    	syscall
    	
    	#dispplay counter
    	li $v0, 1
    	move $a0, $t3
    	syscall
    	
    	li $v0, 4
    	la $a0, Sucess2
    	syscall
    	
    	#ask if they want to play again
    	li $v0, 4
    	la $a0, Again
    	syscall
    	
    	
    	la $a0, answer
    	li $a1, 3
    	li $v0, 8
    	syscall
    	
    	lb $t4, 0($a0)
    	
    	beq $t4, 'y', MAIN 	#take any input of y
    	beq $t4, 'Y', MAIN	#if anything other than y or Y is entered the program will end
    	
    	# epilogue
    	lw $ra, 0($sp)
    	addi $sp, $sp, 4
    	
 
	jr $ra
	
	
INPUTAGAIN:
	
	#have the user guess again
	li $v0, 4
    	la $a0, YourGuess
    	syscall
    	
   	li $v0, 5			# Get input from user
    	syscall
    	move $t1, $v0			#save input in $t1
    	
    	addi $t3, $t3, 1		#inciment counter
    	
    	
    	j DETERMINE
	    	
EXIT:
	#end program
      	li $v0, 4
    	la $a0, newLine
    	syscall
    	
    	li $v0, 10
    	syscall
    	
    	
