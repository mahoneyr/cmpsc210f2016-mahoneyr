.data
	prompt:		.asciiz		"Welcome to this Guessing Game, human!\nPlease pick a number between 1 and 100. Don't tell me what it is.\nMy first guess is 50. How did I do?"
	tooHigh:		.asciiz		"Hmmm, my guess was too high. My next guess is "
	how:		.asciiz		"How did i do?"
	TooLow:		.asciiz		"Ok, my guess was too low. My next guess is "
	TooHigh:		.asciiz		"Ok, my guess was too high. My next guess is "
	Sucess1:		.asciiz		"Yay, I got it in "
	Sucess2:		.asciiz		" tries! Your number was "
	Again:		.asciiz		"\nWould you like to play again? (y/n)"
	answer:		.space 		256
	newLine:		.asciiz		"\n"
	nextGuess:	.asciiz		"\nMy next guess is "
	
.text
MAIN:
	li $v0, 4
	la $a0, newLine
	syscall
	
	li $t0, 50		#computer's guess
	li $t1, 0		#counter
	
	jal WELCOME
	jal INPUT
	jal DETERMINE
	jal EXIT
	
	
WELCOME:
	# prologue
    	subi $sp, $sp, 4
    	sw $ra, 0($sp)

	# procedure body		#prompt the user
    	li $v0, 4
    	la $a0, prompt
    	syscall
    	
    	# epilogue
    	lw $ra, 0($sp)
    	addi $sp, $sp, 4
    	
    	jr $ra
    	
INPUT:
 	# prologue
    	subi $sp, $sp, 4
    	sw $ra, 0($sp)
    	
    	li $v0, 5			# Get input from user
    	syscall
    	move $t3, $v0			#save input in $t3
    	addi $t1, $t1, 1		#inciment counter

    	# epilogue
    	lw $ra, 0($sp)
    	addi $sp, $sp, 4
  
    	jr $ra
    	
DETERMINE:
	  
	 beq $t3, 1, high		#user indicates guess is too high
	 beq $t3, -1, low		#user indicates guess is too low
	 beq $t3, 0, equal		#user indicates guess is correct
	 
	 
low:	#output that guess was too high
	li $v0, 4
   	la $a0, TooHigh
    	syscall
    	
    	sra $t0, $t0, 1		#divide answer by 2
    	
    	beq $t0, 0, under0	#check if answer is under 0

    	
    	#print new answer
    	li $v0, 1
    	move $a0, $t0
    	syscall
    	
    	
   	j INPUTAGAIN
    	
 high:
 	#output guess was too high
	li $v0, 4
    	la $a0, TooLow
    	syscall
    	
    	sra $t4, $t0, 1		#incease guess 
    	add $t0, $t0, $t4
    	
    	
    	bgt $t0, 100, over100	#check if guess went over 100
  
    	#print new answer
    	li $v0, 1
    	move $a0, $t0
    	syscall
    	
   	j INPUTAGAIN
   	
over100:
	li $t0 100	#cap answer at 100
	li $v0, 1
    	move $a0, $t0
    	syscall
    	
    	j INPUTAGAIN
    	
under0:
	li $t0 1	#cap answer at 1
	li $v0, 1
    	move $a0, $t0
    	syscall
    	
    	j INPUTAGAIN

equal:
	# prologue
    	subi $sp, $sp, 4
    	sw $ra, 0($sp)
    	
    	#Display sucess message
	li $v0, 4
    	la $a0, Sucess1
    	syscall
    	
    	#dispplay counter
    	li $v0, 1
    	move $a0, $t1
    	syscall
    	
    	li $v0, 4
    	la $a0, Sucess2
    	syscall
    	
    	#display correct number
    	li $v0, 1
    	move $a0, $t0
    	syscall 
    	
    	#ask if they want to play again
    	li $v0, 4
    	la $a0, Again
    	syscall
    	
    	
    	la $a0, answer		#display correct answer
    	li $a1, 3
    	li $v0, 8
    	syscall
    	
    	lb $t4, 0($a0)
    	
    	beq $t4, 'y', MAIN 	#take any input of y
    	beq $t4, 'Y', MAIN	#if anything other than y or Y is entered the program will end
    	
    	# epilogue
    	lw $ra, 0($sp)
    	addi $sp, $sp, 4
    	
 
	jr $ra
	
INPUTAGAIN:
	
	# prologue
    	subi $sp, $sp, 4
    	sw $ra, 0($sp)
    	
    	li $v0, 5			# Get input from user
    	syscall
    	move $t3, $v0			#save input in $t3
    	addi $t1, $t1, 1		#inciment counter

    	# epilogue
    	lw $ra, 0($sp)
    	addi $sp, $sp, 4
    	
    	j DETERMINE
	
    	
  EXIT:
	#end program
      
    	
    	li $v0, 10
    	syscall
